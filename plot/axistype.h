#ifndef AXIS_TYPE_H
#define AXIS_TYPE_H

enum class AxisType { kNumeric, kText, kTime };

#endif
